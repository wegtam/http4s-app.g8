# http4s web app template

A Giter8 template for creating a web application based upon several libraries
from the Scala ecosystem:

- cats
- cats-effect
- circe
- doobie
- enumeratum
- fs2
- http4s
- pureconfig
- refined
- tapir

The template provides a basic application structure with a database table for
user accounts and routing for setting up an administrator account.
It uses the twirl templating engine and adds bootstrap and jquery via webjars
as dependencies to allow more sophisticated theming options.

This template is intended as a basis for a web application which includes things
like login, logout, administration and serving web pages. If you intend to just
provide a service (e.g. HTTP API using JSON) then this is very likely too heavy.

## Usage

Just use the `new` command of sbt like this:

```
% sbt new https://codeberg.org/wegtam/http4s-app.g8.git
```

## Template license

Written in 2020 by Wegtam GmbH

To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this template to the public domain worldwide.
This template is distributed without any warranty. 

See http://creativecommons.org/publicdomain/zero/1.0/.
