package $package$

import cats.effect._
import cats.syntax.all._
import org.flywaydb.core.Flyway
import org.flywaydb.core.api.output.MigrateResult

/** Provide database migration capabilities using flyway and the given database configuration.
  *
  * @tparam F
  *   A higher kinded type providing needed functionality, which is usually an IO monad like Async or Sync.
  */
final class FlywayDatabaseMigrator[F[_]: Sync](config: DatabaseConfiguration) {

  /** Run the database migrations and the migration result. Please note that this function will
    *
    *   - run within a custom schema (website) which is created by flyway if not existing
    *   - load the migration files from a custom directory within the class path
    *
    * @return
    *   A MigrateResult holding detailed information about the migration run.
    */
  def run(): F[MigrateResult] =
    for {
      flyway <- Sync[F].delay(
        Flyway
          .configure()
          .defaultSchema("website")
          .locations("classpath:db/migration/website")
          .dataSource(config.url.toString, config.user.toString, config.pass.toString)
          .load()
      )
      result <- Sync[F].delay(flyway.migrate())
    } yield result
}
