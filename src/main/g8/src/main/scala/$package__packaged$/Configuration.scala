package $package$

import java.nio.file.Path

import cats._
import cats.syntax.all._
import com.comcast.ip4s._
import org.http4s.Uri
import pureconfig._

import scala.util.matching.Regex

opaque type CookieName = String
object CookieName {
  given ConfigReader[CookieName] = ConfigReader.fromStringOpt(CookieName.from)

  /** Create an instance of CookieName from the given String type.
    *
    * @param source
    *   An instance of type String which will be returned as a CookieName.
    * @return
    *   The appropriate instance of CookieName.
    */
  def apply(source: String): CookieName = source

  /** Try to create an instance of CookieName from the given String.
    *
    * @param source
    *   A String that should fulfil the requirements to be converted into a CookieName.
    * @return
    *   An option to the successfully converted CookieName.
    */
  def from(source: String): Option[CookieName] = Option(source).map(_.trim).filter(_.nonEmpty)
}

opaque type JdbcDriverName = String
object JdbcDriverName {
  given ConfigReader[JdbcDriverName] = ConfigReader.fromStringOpt(JdbcDriverName.from)

  val Format: Regex = "^\\\\w+\\\\.[\\\\w\\\\d\\\\.]+[\\\\w\\\\d]+\$".r

  /** Create an instance of JdbcDriverName from the given String type.
    *
    * @param source
    *   An instance of type String which will be returned as a JdbcDriverName.
    * @return
    *   The appropriate instance of JdbcDriverName.
    */
  def apply(source: String): JdbcDriverName = source

  /** Try to create an instance of JdbcDriverName from the given String.
    *
    * @param source
    *   A String that should fulfil the requirements to be converted into a JdbcDriverName.
    * @return
    *   An option to the successfully converted JdbcDriverName.
    */
  def from(source: String): Option[JdbcDriverName] = Option(source).filter(Format.matches)
}

opaque type JdbcPassword = String
object JdbcPassword {
  given ConfigReader[JdbcPassword] = ConfigReader.fromStringOpt(JdbcPassword.from)

  /** Create an instance of JdbcPassword from the given String type.
    *
    * @param source
    *   An instance of type String which will be returned as a JdbcPassword.
    * @return
    *   The appropriate instance of JdbcPassword.
    */
  def apply(source: String): JdbcPassword = source

  /** Try to create an instance of JdbcPassword from the given String.
    *
    * @param source
    *   A String that should fulfil the requirements to be converted into a JdbcPassword.
    * @return
    *   An option to the successfully converted JdbcPassword.
    */
  def from(source: String): Option[JdbcPassword] = Option(source)
}

opaque type JdbcUrl = String
object JdbcUrl {
  given ConfigReader[JdbcUrl] = ConfigReader.fromStringOpt(JdbcUrl.from)

  val Format: Regex = "^jdbc:[a-zA-z0-9]+:.*".r

  /** Create an instance of JdbcUrl from the given String type.
    *
    * @param source
    *   An instance of type String which will be returned as a JdbcUrl.
    * @return
    *   The appropriate instance of JdbcUrl.
    */
  def apply(source: String): JdbcUrl = source

  /** Try to create an instance of JdbcUrl from the given String.
    *
    * @param source
    *   A String that should fulfil the requirements to be converted into a JdbcUrl.
    * @return
    *   An option to the successfully converted JdbcUrl.
    */
  def from(source: String): Option[JdbcUrl] = Option(source).filter(Format.matches)
}

opaque type JdbcUser = String
object JdbcUser {
  given ConfigReader[JdbcUser] = ConfigReader.fromStringOpt(JdbcUser.from)

  /** Create an instance of JdbcUser from the given String type.
    *
    * @param source
    *   An instance of type String which will be returned as a JdbcUser.
    * @return
    *   The appropriate instance of JdbcUser.
    */
  def apply(source: String): JdbcUser = source

  /** Try to create an instance of JdbcUser from the given String.
    *
    * @param source
    *   A String that should fulfil the requirements to be converted into a JdbcUser.
    * @return
    *   An option to the successfully converted JdbcUser.
    */
  def from(source: String): Option[JdbcUser] = Option(source).map(_.trim).filter(_.nonEmpty)
}

/** The main configuration for the application.
  *
  * @param database
  *   Settings needed to access the database.
  * @param server
  *   All settings related to the web server / service.
  */
final case class Configuration(database: DatabaseConfiguration, server: ServerConfiguration)

object Configuration {
  given Eq[Configuration] = Eq.fromUniversalEquals

  given ConfigReader[Configuration] = ConfigReader.forProduct2("database", "server")(Configuration.apply)
}

/** Settings for controlling a possible HTTP API.
  *
  * @param enabled
  *   The whole API can be disabled by setting this to false.
  * @param documentation
  *   If this flag is set to true then the API documentation will be auto generated and hosted via Swagger UI.
  * @param host
  *   The hostname on which the API shall listen for requests.
  * @param port
  *   The TCP port number on which the API shall listen for requests.
  */
final case class ApiConfiguration(enabled: Boolean, documentation: Boolean, host: Host, port: Port)

object ApiConfiguration {
  given Eq[ApiConfiguration] = Eq.fromUniversalEquals

  given ConfigReader[Host] = ConfigReader.fromStringOpt[Host](Host.fromString)
  given ConfigReader[Port] = ConfigReader.fromStringOpt[Port](Port.fromString)

  given ConfigReader[ApiConfiguration] =
    ConfigReader.forProduct4("enabled", "documentation", "host", "port")(ApiConfiguration.apply)
}

/** These settings are needed to properly initialise the CSRF protection middleware of http4s.
  *
  * @param cookieName
  *   The name of the cookie used for the CSRF protection part.
  * @param keyFile
  *   A file which contains the key used to build the CSRF protection. If unset the key will only be held in memory.
  * @param host
  *   The hostname that will be checked against.
  * @param port
  *   If set requests will be checked against the given port number.
  * @param scheme
  *   The used url protocol scheme which will be checked against.
  */
final case class CsrfProtectionConfiguration(
    cookieName: CookieName,
    keyFile: Option[Path],
    host: Host,
    port: Option[Port],
    scheme: Uri.Scheme
)

object CsrfProtectionConfiguration {
  given Eq[CsrfProtectionConfiguration] = Eq.fromUniversalEquals

  given ConfigReader[Host]       = ConfigReader.fromStringOpt[Host](Host.fromString)
  given ConfigReader[Port]       = ConfigReader.fromStringOpt[Port](Port.fromString)
  given ConfigReader[Uri.Scheme] = ConfigReader.fromStringOpt(s => Uri.Scheme.fromString(s).toOption)

  given ConfigReader[CsrfProtectionConfiguration] =
    ConfigReader.forProduct5("cookie-name", "key-file", "host", "port", "scheme")(CsrfProtectionConfiguration.apply)
}

/** Settings needed to access the database.
  *
  * @param driver
  *   The class name of the JDBC driver to be used.
  * @param url
  *   A JDBC connection url **without** credentials.
  * @param user
  *   The username for the database login.
  * @param pass
  *   The password for the database login.
  */
final case class DatabaseConfiguration(driver: JdbcDriverName, url: JdbcUrl, user: JdbcUser, pass: JdbcPassword)

object DatabaseConfiguration {
  given Eq[DatabaseConfiguration] = Eq.fromUniversalEquals

  given ConfigReader[DatabaseConfiguration] =
    ConfigReader.forProduct4("driver", "url", "user", "pass")(DatabaseConfiguration.apply)
}

/** The server configuration should contain all settings regarding the behaviour of http4s.
  *
  * @param host
  *   The hostname on which the service shall listen for requests.
  * @param port
  *   The TCP port number on which the service shall listen for requests.
  * @param api
  *   Settings for controlling a possible HTTP API.
  * @param csrf
  *   Settings to properly configure the CSRF protection middleware of http4s.
  * @param assetsPath
  *   The path under which static assets shall be served.
  */
final case class ServerConfiguration(
    host: Host,
    port: Port,
    api: ApiConfiguration,
    csrf: CsrfProtectionConfiguration,
    assetsPath: Uri.Path
)

object ServerConfiguration {
  given Eq[ServerConfiguration] = Eq.fromUniversalEquals

  given ConfigReader[Host]     = ConfigReader.fromStringOpt[Host](Host.fromString)
  given ConfigReader[Port]     = ConfigReader.fromStringOpt[Port](Port.fromString)
  given ConfigReader[Uri.Path] = ConfigReader.fromStringOpt(string => Uri.Path.unsafeFromString(string).some)

  given ConfigReader[ServerConfiguration] =
    ConfigReader.forProduct5("host", "port", "api", "csrf", "assets-path")(ServerConfiguration.apply)
}
