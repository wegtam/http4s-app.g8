package $package$

import cats.arrow._
import cats.effect._
import cats.syntax.all._
import com.typesafe.config.ConfigFactory
import $package$.api.HttpApi
import org.http4s._
import org.http4s.ember.server._
import org.http4s.implicits._
import org.http4s.server._
import org.http4s.server.middleware.CSRF
import org.http4s.server.staticcontent.resourceServiceBuilder
import org.slf4j.LoggerFactory
import pureconfig._
import sttp.apispec.openapi.OpenAPI
import sttp.apispec.openapi.circe.yaml._
import sttp.tapir.docs.openapi.OpenAPIDocsInterpreter
import sttp.tapir.server.http4s.Http4sServerInterpreter
import sttp.tapir.swagger.SwaggerUI

object Server extends IOApp {
  private final val log = LoggerFactory.getLogger(getClass)

  override def run(args: List[String]): IO[ExitCode] =
    for {
      _ <- IO(log.debug(s"Running version: \${BuildInfo.version} compiled with Scala \${BuildInfo.scalaVersion}."))
      configuration <- IO(
        ConfigSource.fromConfig(ConfigFactory.load(getClass.getClassLoader)).loadOrThrow[Configuration]
      )
      clock            = java.time.Clock.systemUTC()
      databaseMigrator = new FlywayDatabaseMigrator[IO](configuration.database)
      migrationResult <- databaseMigrator.run()
      _ <- migrationResult.migrationsExecuted match {
        case 0 =>
          IO(log.debug(s"Database schema \${migrationResult.schemaName} up to date, no migrations applied."))
        case _ =>
          IO(
            log.debug(
              s"Applied \${migrationResult.migrationsExecuted} migrations to database schema \${migrationResult.schemaName} to migrate from \${migrationResult.initialSchemaVersion} to \${migrationResult.targetSchemaVersion}."
            )
          )
      }
      csrfKey <- CSRF.generateSigningKey[IO]()
      csrfBuilder = CSRF[IO, IO](
        csrfKey,
        CSRF.defaultOriginCheck(
          _,
          configuration.server.csrf.host.toString,
          configuration.server.csrf.scheme,
          configuration.server.csrf.port.map(_.value)
        )
      )
      rootUri = Uri(
        scheme = configuration.server.csrf.scheme.some,
        authority = Uri
          .Authority(
            host = Uri.Host.fromIp4sHost(configuration.server.csrf.host),
            port = configuration.server.csrf.port.map(_.value).filterNot(port => port === 80 || port === 443)
          )
          .some,
        path = Uri.Path.unsafeFromString("/")
      )
      csrfMiddleware = csrfBuilder
        .withClock(clock)
        .withCookieDomain(configuration.server.csrf.host.toString.some)
        .withCookieName(configuration.server.csrf.cookieName.toString)
        .withCookiePath("/".some)
        .withCSRFCheck(
          CSRF.checkCSRFinHeaderAndForm[IO, IO](configuration.server.csrf.cookieName.toString, FunctionK.id)
        )
        .withOnFailure(
          Response[IO](headers = Headers(List(headers.Location(rootUri))), status = Status.SeeOther)
            .removeCookie(
              configuration.server.csrf.cookieName.toString
            ) // Redirect user to frontpage and remove cookie. For better solution store csrf key on disk.
        )
        .build
      assetsRoutes = resourceServiceBuilder[IO](configuration.server.assetsPath.toAbsolute.toString).toRoutes
      demoRoutes   = new DemoRoutes[IO]
      websiteRouter = Router(
        configuration.server.assetsPath.toAbsolute.toString -> assetsRoutes,
        "/"                                                 -> demoRoutes.routes
      ).orNotFound
      websiteResource = EmberServerBuilder
        .default[IO]
        .withHost(configuration.server.host)
        .withPort(configuration.server.port)
        .withHttpApp(csrfMiddleware.validate()(websiteRouter))
        .build
      webServer = websiteResource.use(server =>
        IO(log.info(s"HTTP server startet at \${server.address}")) >> IO.never.as(ExitCode.Success)
      )
      // We keep our API routes seperately because we don't want (and can't) CSRF protection on our API.
      httpApiResource =
        if (configuration.server.api.enabled) {
          val apiRoutes = new HttpApi[IO]
          val openApiDocs =
            if (configuration.server.api.documentation)
              OpenAPIDocsInterpreter().toOpenAPI(List(HttpApi.echo), "My API", BuildInfo.version).some
            else
              none[OpenAPI]
          val httpApiRouter = openApiDocs.fold(Router("/api/v1" -> apiRoutes.routes).orNotFound)(docs =>
            Router(
              "/api/v1" -> apiRoutes.routes,
              "/"       -> Http4sServerInterpreter[IO]().toRoutes(SwaggerUI[IO](docs.toYaml))
            ).orNotFound
          )
          val httpApiResource = EmberServerBuilder
            .default[IO]
            .withHost(configuration.server.api.host)
            .withPort(configuration.server.api.port)
            .withHttpApp(httpApiRouter)
            .build
          httpApiResource.use(server =>
            IO(log.info(s"API server startet at \${server.address}")) >> IO.never.as(ExitCode.Success)
          )
        } else {
          IO.unit.as(ExitCode.Success)
        }
      executeFibers <- (webServer, httpApiResource).parTupled // Run both fibers in parallel.
      (exitCode, _) = executeFibers
    } yield exitCode

}
