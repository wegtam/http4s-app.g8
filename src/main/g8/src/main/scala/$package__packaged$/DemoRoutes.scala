package $package$

import cats._
import cats.effect._
import cats.syntax.all._
import io.circe._
import io.circe.generic.semiauto._
import org.http4s._
import org.http4s.dsl._
import org.http4s.twirl.TwirlInstances._
import org.slf4j.LoggerFactory

final class DemoRoutes[F[_]: Async] extends Http4sDsl[F] {
  private final val log = LoggerFactory.getLogger(getClass)

  private val index: HttpRoutes[F] = HttpRoutes.of { case req @ GET -> Root =>
    for {
      _        <- Sync[F].delay(log.debug(s"Handling request \$req"))
      response <- Ok(views.html.index())
    } yield response
  }

  val routes = index
}
