package $package$

import java.net.ServerSocket
import java.sql.{ Connection, DriverManager }

import cats._
import cats.effect._
import cats.syntax.all._
import com.comcast.ip4s._
import com.typesafe.config.ConfigFactory
import org.flywaydb.core._
import pureconfig._

import munit._

/** A base class for writing tests that need a database.
  *
  * **Attention!**
  *
  *   - It will delete and re-create the configured database before and after all tests!
  *   - It will clean the database in between tests!
  */
abstract class TestBase extends CatsEffectSuite {
  final val RequiresDatabase = new munit.Tag("RequiresDatabase")

  // Load the configuration and throw an exception if anything is wrong with it.
  protected final val configuration: Configuration =
    ConfigSource.fromConfig(ConfigFactory.load(getClass.getClassLoader)).loadOrThrow[Configuration]
  // A flyway instance which is configured to allow cleaning the database.
  protected final val flyway = Flyway
    .configure()
    .defaultSchema("website")
    .dataSource(
      configuration.database.url.toString,
      configuration.database.user.toString,
      configuration.database.pass.toString
    )
    .cleanDisabled(false)
    .load()

  /** Create a resource with a database connection. The connection is created with the given configuration but the
    * database name will be replaced with the generic "template1" database name that should always be present in
    * PostgreSQL databases if the flag `ignoreDatabaseName` is set to true.
    *
    * @param config
    *   A database configuration containing the connection information.
    * @param ignoreDatabaseName
    *   A flag that will cause the database name being replaced with "template1" if set to true.
    * @return
    *   A resource containing a database connection.
    */
  protected def createDatabaseConnection(
      config: DatabaseConfiguration,
      ignoreDatabaseName: Boolean
  ): Resource[IO, Connection] = {
    val databaseUrl =
      if (ignoreDatabaseName) {
        val databaseName = config.url.toString.split("/").reverse.take(1).mkString
        config.url.toString.replace(databaseName, "template1")
      } else {
        config.url.toString
      }
    Resource.make(IO(DriverManager.getConnection(databaseUrl, config.user.toString, config.pass.toString)))(
      connection => IO(connection.close())
    )
  }

  /** Determine a free local port for a socket.
    *
    * @return
    *   An option to a free port number.
    */
  protected def findFreePort(): Option[Port] = {
    val socket = new ServerSocket(0)
    val port   = socket.getLocalPort
    socket.setReuseAddress(true) // Allow instant rebinding of the socket.
    socket.close()               // Free the socket for further use by closing it.
    Port.fromInt(port)
  }

  override def afterAll(): Unit =
    createDatabaseConnection(configuration.database, ignoreDatabaseName = true)
      .use { connection =>
        val databaseName = configuration.database.url.toString.split("/").reverse.take(1).mkString
        for {
          statement <- IO(connection.prepareStatement("DROP DATABASE IF EXISTS ?"))
          _         <- IO(statement.setString(1, databaseName))
          _         <- IO(statement.executeUpdate())
          _         <- IO(statement.close())
        } yield ()
      }
      .unsafeRunSync()

  override def afterEach(context: AfterEach): Unit = {
    val _ = flyway.clean()
  }

  override def beforeAll(): Unit =
    createDatabaseConnection(configuration.database, ignoreDatabaseName = true)
      .use { connection =>
        val databaseName = configuration.database.url.toString.split("/").reverse.take(1).mkString
        for {
          checkStatement <- IO(
            connection.prepareStatement("SELECT datname FROM pg_catalog.pg_database WHERE datname = ?")
          )
          _               <- IO(checkStatement.setString(1, databaseName))
          createStatement <- IO(connection.prepareStatement("CREATE DATABASE ?"))
          _               <- IO(createStatement.setString(1, databaseName))
          check           <- IO(checkStatement.executeQuery())
          _ <-
            if (check.next())
              IO.pure(0) // Database already exists.
            else
              IO(createStatement.executeUpdate())
          _ <- IO(checkStatement.close())
          _ <- IO(createStatement.close())
        } yield ()
      }
      .unsafeRunSync()

  override def beforeEach(context: BeforeEach): Unit = {
    val _ = flyway.migrate()
  }
}
