/*
 * Copyright (C) 2022  Contributors as noted in the AUTHORS.md file
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.http4s
package twirl

import _root_.play.twirl.api._
import org.http4s.Charset.`UTF-8`
import org.http4s.headers.`Content-Type`

@SuppressWarnings(Array("scalafix:DisableSyntax.defaultArgs"))
object TwirlInstances {
  implicit def htmlContentEncoder[F[_]](implicit
      charset: Charset = `UTF-8`
  ): EntityEncoder[F, Html] =
    contentEncoder(MediaType.text.html)

  /** Note: Twirl uses a media type of `text/javascript`.  This is obsolete, so we instead return
    * `application/javascript`.
    */
  implicit def jsContentEncoder[F[_]](implicit
      charset: Charset = `UTF-8`
  ): EntityEncoder[F, JavaScript] =
    contentEncoder(MediaType.application.javascript)

  implicit def xmlContentEncoder[F[_]](implicit charset: Charset = `UTF-8`): EntityEncoder[F, Xml] =
    contentEncoder(MediaType.application.xml)

  implicit def txtContentEncoder[F[_]](implicit charset: Charset = `UTF-8`): EntityEncoder[F, Txt] =
    contentEncoder(MediaType.text.plain)

  private def contentEncoder[F[_], C <: Content](
      mediaType: MediaType
  )(implicit charset: Charset): EntityEncoder[F, C] =
    EntityEncoder
      .stringEncoder[F]
      .contramap[C](content => content.body)
      .withContentType(`Content-Type`(mediaType, charset))
}
