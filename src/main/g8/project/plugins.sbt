// Compiler plugins
addCompilerPlugin("org.scalameta" % "semanticdb-scalac" % "4.7.8" cross CrossVersion.full)
// Regular plugins
addSbtPlugin("com.eed3si9n"      % "sbt-buildinfo"       % "0.11.0")
addSbtPlugin("com.github.sbt"    % "sbt-dynver"          % "5.0.1")
addSbtPlugin("de.heikoseeberger" % "sbt-header"          % "5.9.0")
addSbtPlugin("com.github.sbt"    % "sbt-native-packager" % "1.9.16")
addSbtPlugin("io.spray"          % "sbt-revolver"        % "0.9.1")
addSbtPlugin("ch.epfl.scala"     % "sbt-scalafix"        % "0.10.4")
addSbtPlugin("org.scalameta"     % "sbt-scalafmt"        % "2.5.0")
addSbtPlugin("org.scoverage"     % "sbt-scoverage"       % "2.0.7")
addSbtPlugin("com.typesafe.play" % "sbt-twirl"           % "1.6.0-RC4")
// Needed to build debian packages via java (for sbt-native-packager).
libraryDependencies += "org.vafer" % "jdeb" % "1.10" artifacts (Artifact("jdeb", "jar", "jar"))
